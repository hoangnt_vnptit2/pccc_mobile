import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-services-price',
  templateUrl: 'services-price.html',
})
export class ServicesPricePage {

  data: any;
  id: number;
  note: any;
  txtcounter: number;
  isEdit = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
    this.data = this.navParams.get('dt');
    this.id = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicesPricePage');
  }

  ionViewDidEnter() {
    console.log(this.data);
    console.log(this.id);
  }

  acceptOrders() {
    this.viewCtrl.dismiss();
  }

  txtMaxLengthCheck(e) {
    this.txtcounter = e.length

  }

  editOrders(){
    this.isEdit=true;
  }

  cancelOrders(){

  }

  submit(){
    
  }

}
