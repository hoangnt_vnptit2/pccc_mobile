import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { orders } from '../../services/order';
import { DetailHistoryPage } from '../detail-history/detail-history';

@Component({
  selector: 'page-history-book',
  templateUrl: 'history-book.html',
})
export class HistoryBookPage {

  orders = orders.data;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryBookPage');
  }

  toDetail(v: number) {
    var data = this.orders[v];
    this.navCtrl.push(DetailHistoryPage, { dt: data });
  }
}
