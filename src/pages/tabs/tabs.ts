import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HistoryBookPage } from '../history-book/history-book'; 
import { NotificationPage } from '../notification/notification';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tabHomeRoot: any = HomePage;
  tabHistoryRoot: any = HistoryBookPage;
  tabNotificationRoot:any = NotificationPage;
  tabContactRoot:any = ContactPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
