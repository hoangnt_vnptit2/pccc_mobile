import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { services } from '../../services/services';
import { BookPage } from '../book/book';
import { ServiceProvider } from '../../services/GlobalVar';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  data: any = {};
  services = services[0].data;
  constructor(public navCtrl: NavController, public navParams: NavParams, public serviceProvider: ServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ionViewDidEnter() {
    this.data.image = "https://i.imgur.com/U4Oadyu.png";
    this.data.image2 = "https://i.imgur.com/r7uK5St.jpg";
    console.log(this.services);
    // localStorage.removeItem('BookData');
  }

  goBookPage(service:any)
  {
    this.serviceProvider.selectedServiceList=[];
    this.serviceProvider.selectedServiceList.push(service);
    this.navCtrl.push(BookPage);
  }

}
