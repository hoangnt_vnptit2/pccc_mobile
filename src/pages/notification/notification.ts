import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { orders } from '../../services/order';
import { ServicesPricePage } from '../services-price/services-price';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  orders = orders.data;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modal: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
    console.log(this.orders);

  }

  toServicesPrice(v: number) {
    var data = this.orders[v];
    let myModal = this.modal.create(ServicesPricePage, { dt: data, id: v });
    // myModal.onDidDismiss(data => {
    //   console.log(data);
    // })
    myModal.present();
  }



}
