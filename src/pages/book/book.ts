import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { services } from '../../services/services';
import { ServiceProvider } from '../../services/GlobalVar';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture';
import { Camera, CameraOptions } from '@ionic-native/camera';
import moment from 'moment';
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";

@Component({
  selector: 'page-book',
  templateUrl: 'book.html',
})
export class BookPage {
  @ViewChild(Slides) slides: Slides;
  // @ViewChild('myvideo') myVideo:any;
  services = services[0].data;
  selectedService = [];
  txtcounter: number;
  note = "";
  imageResponse =[];
  video = '';
  phone: string;
  name: string;
  address: string;
  selectedRadio: string = "cash";
  payments = [
    { paymentType: 'cash', paymentStatusCode: 0, paymentStatusName: 'Trả sau bằng tiền mặt' },
    { paymentType: 'card', paymentStatusCode: 1, paymentStatusName: 'Trả trước qua cổng thanh toán' }
  ];
  // datetime: String = new Date().toISOString();
  // datetime: String = new Date(new Date().setHours(new Date().getHours()-17)).toISOString();
  datetime = moment().format();
  serviceIsNULL = false;
  dateTimeErr = false;
  nameErr = false;
  phoneErr = false;
  nameIsNULL = false;
  phoneIsNULL = false;
  addressIsNULL = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public serviceProvider: ServiceProvider, public imagePicker: ImagePicker,
    public file: File, private mediaCapture: MediaCapture, public camera: Camera,
    public toastController: ToastController,
    private sanitizer:DomSanitizer) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookPage');
    this.slides.lockSwipeToNext(true);
    this.selectedService = this.serviceProvider.selectedServiceList[0].serviceID;
  }

  nextStep2() {

    console.log(this.checkDateTime());
    if (this.dateTimeErr == false && this.serviceIsNULL == false) {
      this.slides.lockSwipeToNext(false);
      this.slides.lockSwipeToPrev(false);
      this.slides.slideTo(this.slides.getActiveIndex() + 1);
      this.slides.lockSwipeToNext(true);
    }

  }

  nextStep() {
    if (this.name == null) {
      this.nameIsNULL = true;
    }

    if (this.phone == null) {
      this.phoneIsNULL = true;
    }

    if (this.address == null) {
      this.addressIsNULL = true;
    }

    if (this.addressIsNULL == false && this.nameIsNULL == false && this.addressIsNULL == false && this.nameErr == false && this.phoneErr == false) {
      this.slides.lockSwipeToNext(false);
      this.slides.lockSwipeToPrev(false);
      this.slides.slideTo(this.slides.getActiveIndex() + 1);
      this.slides.lockSwipeToNext(true);
      console.log(this.checkDateTime());
      console.log(this.phone);
    }
  }

  changeServices() {
    this.serviceProvider.selectedServiceList = [];
    for (var i = 0; i < this.selectedService.length; i++) {
      for (var j = 0; j < this.services.length; j++) {
        if (this.selectedService[i] == this.services[j].serviceID) {
          this.serviceProvider.selectedServiceList.push(this.services[j]);
          break;
        }
      }
    }

    if (this.serviceProvider.selectedServiceList.length == 0) {
      this.serviceIsNULL = true;
    }
    else {
      this.serviceIsNULL = false;
    }
    console.log(this.serviceProvider.selectedServiceList);
  }

  checkDateTime() {
    var currDateTime = moment(moment().format());
    var diffTime = moment.duration(currDateTime.diff(this.datetime)).asMilliseconds();
    console.log(currDateTime);
    console.log(this.datetime);
    console.log(diffTime);

    if (diffTime > 15000) {
      this.dateTimeErr = true;
      // this.datetime = moment().format();
    }
    else {
      this.dateTimeErr = false;
    }
  }

  checkName() {
    var nameArr = [];
    var nameArrNew = this.name.split('');
    let isNum = 0;
    let isSymbol = 0;

    if (nameArrNew.length == 0) {
      this.nameIsNULL = true;
    }
    else {
      this.nameIsNULL = false;
    }

    for (var i = 0; i < nameArrNew.length; i++) {
      if (nameArrNew[i] != " ") {
        nameArr.push(nameArrNew[i]);
      }
    }
    for (var i = 0; i < nameArr.length; i++) {
      if (isNaN(Number(nameArr[i].toString())) == false) {
        isNum += 1;
      }
      if (/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(nameArr[i])) {
        isSymbol += 1;
      }
    }

    if (isNum > 0 || isSymbol > 0) {
      this.nameErr = true;
    }
    else {
      this.nameErr = false;
      isSymbol = 0;
      isNum = 0;
    }

  }

  checkPhone() {
    var phoneArr = this.phone.split('');

    if (phoneArr.length == 0) {
      this.phoneIsNULL = true;
    }
    else {
      this.phoneIsNULL = false;
    }

    if (phoneArr.length != 10 && phoneArr.length != 0) {
      this.phoneErr = true;
    }
    else {
      this.phoneErr = false;
    }

  }

  checkAddress() {
    var addressArr = this.address.split('');

    if (addressArr.length == 0) {
      this.addressIsNULL = true;
    }
    else {
      this.addressIsNULL = false;
    }
  }

  txtMaxLengthCheck(e) {
    this.txtcounter = e.length

  }


  // getVideo(){
  //   let video = this.myVideo.nativeElement;
  //   var options={
  //     sourceType:2,
  //     mediaType:1
  //   };

  //   this.camera.getPicture(options).then((data)=>{
  //     video.src=data;
  //     video.play();
  //   }, (err) => {
  //         // Handle error
  //       });
  // }

  readVideoFileasGeneral(data_uri) {
    if (!data_uri.includes('file://')) {
      data_uri = 'file://' + data_uri;
    }
    return this.file.resolveLocalFilesystemUrl(data_uri)
      .then((entry: any) => {
        //***it does not get in here***
        // this.presentQuickToastMessage(data_uri); 
        return new Promise((resolve) => {//, reject) => { 
          entry.file((file) => {
            let fileReader = new FileReader();
            fileReader.onloadend = () => {
              let blob = new Blob([fileReader.result], { type: file.type });
              resolve({ blob: blob, file: file });
            };
            fileReader.readAsArrayBuffer(file);
          });
        })
      })
      .catch((error) => {
        // this.presentQuickToastMessage(error); 
        //***it presents "plugin_not_installed" here***
      });
  }

  changePayment() {
    console.log(this.payments);
    console.log(this.selectedRadio)
  }

  saveBook() {
    console.log(moment(this.datetime).format('MM/DD/YYYY, h:mm a'))
    var value = JSON.parse(localStorage.getItem('BookData')) || [];
    var chosenServices = [];

    for (var i = 0; i < this.serviceProvider.selectedServiceList.length; i++) {
      chosenServices.push(this.serviceProvider.selectedServiceList[i].serviceID)
    }

    value.push({
      chosenServices: chosenServices,
      expectedTime: moment(this.datetime).format('MM/DD/YYYY, h:mm a'),
      customerAddress: this.address,
      customerName: this.name,
      customerPhone: this.phone,
      note: this.note,
      paymentType: this.selectedRadio,
      deviceID: "5545645454"
    });

    localStorage.setItem('BookData', JSON.stringify(value));

    let toast = this.toastController.create({
      message: 'Đặt lịch thành công',
      duration: 3000
    });
    toast.present();
    console.log("dt", JSON.parse(localStorage.getItem('BookData')) || []);

    this.navCtrl.pop();
  }

  //Các hàm xử lý ảnh, video
  takePicture() {
    let options: CameraOptions = {
      quality: 0,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    if (Array.isArray(this.imageResponse)) {
      if (this.imageResponse.length == 10) {
        alert(
          "Bạn chỉ được gửi tối đa 10 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
        );
      } else {
        this.camera.getPicture(options).then(
          imageData => {
            console.log('file uri: ' + imageData)
            this.imageResponse.push(imageData)
          },
          err => {
            console.log(err);
          }
        );
      }
    }
    else {
      this.camera.getPicture(options).then(
        imageData => {
          console.log('file uri: ' + imageData)
        },
        err => {
          console.log(err);
        }
      );
    }

  }
  recordVideo() {
    if (this.video != "")
      alert(
        "Bạn chỉ được gửi tối đa 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      let options: CaptureVideoOptions = { limit: 1, quality: 0 };
      this.mediaCapture.captureVideo(options).then(
        (data: MediaFile[]) => {
          console.log(data);
          var i, path, len;
          for (i = 0, len = data.length; i < len; i += 1) {
            this.video = data[i].fullPath;
            console.log(this.video)
          }
        },
        (err: CaptureError) => {
          console.error(err);
        }
      );
    }
  }
  getImages() {
    if (this.imageResponse.length == 10) {
      alert(
        "Bạn chỉ được gửi tối đa 10 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    } else {
      let count = 10 - this.imageResponse.length;
      let options: ImagePickerOptions = {
        quality: 0,
        outputType: 0,
        maximumImagesCount: count
      };
      this.imagePicker.getPictures(options).then(
        results => {
          for (let pic of results) {
            this.imageResponse.push(pic); 
          }
          console.log(this.imageResponse)
        },
        err => {
          console.log(err)
        }
      );    
    }
  }
  delPic(pic, index)
  {
    console.log('ảnh cần xoá: ' + pic)
    this.imageResponse.splice(index, 1);
  }
  getVideo() {
    console.log("video");
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.VIDEO
    };

    this.camera.getPicture(options).then((data) => {
      console.log(data)
      this.video=data
    }, (err) => {
      console.log(err)
    });
  }
  delVideo()
  {
    this.video=''
  }
  fixDisplayURLFromFileURI(url: string) {
    let win: any = window;
    return win.Ionic.WebView.convertFileSrc(url)
  }
}
