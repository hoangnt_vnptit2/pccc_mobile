import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { company } from '../../services/company';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  company = company;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

}
