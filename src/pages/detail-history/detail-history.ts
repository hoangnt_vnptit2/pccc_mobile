import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-detail-history',
  templateUrl: 'detail-history.html',
})
export class DetailHistoryPage {
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.data = navParams.get('dt');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailHistoryPage');
    console.log(this.data);
  }

}
