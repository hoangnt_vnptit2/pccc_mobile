export const orders =
    {
    "code": 0,
    "message": "sucess",
    "data": [
      {
        "chosenServices": [
          "serviceID1, serviceID2"
        ],
        "expectedTime": "12/14/2019, 12:37:21 PM",
        "customerAddress": "42 pham ngoc thach",
        "customerName": "nguyen van a",
        "customerPhone": "0900000001",
        "note": "vui long gọi truoc 30p khi den",
        "payment": {
          "paymentType": "cash",
          "paymentStatusCode": 0,
          "paymentStatusName": "chưa thanh toán",
          "paymentDate": ""
        },
        "order": {
          "orderStatusCode": 0,
          "orderStatusName": "Đã đặt, chưa chưa báo giá",
          "orderID": "123456",
          "orderValue": {
            "sum": "1,900,000",
            "details": [
              {
                "sericeID": "123456",
                "serviceName": "bảo trì máy lạnh",
                "charge": "1,000,000"
              },
              {
                "sericeID": "123456",
                "serviceName": "Sửa điện thoại",
                "charge": "900,000"
              }
            ]
          }
        },
        "imgList": [
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg"
        ],
        "videoList": [
          "https://brand.assets.adidas.com/video/upload/q_auto,vc_auto/video/upload/global%20brand%20publishing/Running/UB20/running-ss20-ub20-launch-catLP-dual-mh-medium-t.mp4"
        ],
        "assigner": {
          "assignerName": "",
          "assignerID": "",
          "assineePhone": "",
          "assignDate": "12/14/2019, 12:37:21 PM"
        },
        "assignee": {
          "assigneeName": "",
          "assigneeID": "",
          "assigneePhone": "",
          "avatar": "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "workDate": "12/14/2019, 12:37:21 PM"
        }
      },
      {
        "chosenServices": [
          "serviceID1, serviceID2"
        ],
        "expectedTime": "12/14/2019, 12:37:21 PM",
        "customerAddress": "42 pham ngoc thach",
        "customerName": "nguyen van a",
        "customerPhone": "0900000001",
        "note": "vui long gọi truoc 30p khi den",
        "payment": {
          "paymentType": "cash",
          "paymentStatusCode": 0,
          "paymentStatusName": "chưa thanh toán",
          "paymentDate": ""
        },
        "order": {
          "orderStatusCode": 1,
          "orderStatusName": "Đã báo giá, chưa phản hồi",
          "orderID": "123456",
          "orderValue": {
            "sum": "1,900,000",
            "details": [
              {
                "sericeID": "123456",
                "serviceName": "bảo trì máy lạnh",
                "charge": "1,000,000"
              },
              {
                "sericeID": "123456",
                "serviceName": "Sửa điện thoại",
                "charge": "900,000"
              }
            ]
          }
        },
        "imgList": [
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg"
        ],
        "videoList": [
          "https://brand.assets.adidas.com/video/upload/q_auto,vc_auto/video/upload/global%20brand%20publishing/Running/UB20/running-ss20-ub20-launch-catLP-dual-mh-medium-t.mp4"
        ],
        "assigner": {
          "assignerName": "",
          "assignerID": "",
          "assineePhone": "",
          "assignDate": "12/14/2019, 12:37:21 PM"
        },
        "assignee": {
          "assigneeName": "",
          "assigneeID": "",
          "assigneePhone": "",
          "avatar": "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "workDate": "12/14/2019, 12:37:21 PM"
        }
      },
      {
        "chosenServices": [
          "serviceID1, serviceID2"
        ],
        "expectedTime": "12/14/2019, 12:37:21 PM",
        "customerAddress": "42 pham ngoc thach",
        "customerName": "nguyen van a",
        "customerPhone": "0900000001",
        "note": "vui long gọi truoc 30p khi den",
        "payment": {
          "paymentType": "cash",
          "paymentStatusCode": 0,
          "paymentStatusName": "chưa thanh toán",
          "paymentDate": ""
        },
        "order": {
          "orderStatusCode": 2,
          "orderStatusName": "Cancel",
          "orderID": "123456",
          "orderValue": {
            "sum": "1,900,000",
            "details": [
              {
                "sericeID": "123456",
                "serviceName": "bảo trì máy lạnh",
                "charge": "1,000,000"
              },
              {
                "sericeID": "123456",
                "serviceName": "Sửa điện thoại",
                "charge": "900,000"
              }
            ]
          }
        },
        "imgList": [
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg"
        ],
        "videoList": [
          "https://brand.assets.adidas.com/video/upload/q_auto,vc_auto/video/upload/global%20brand%20publishing/Running/UB20/running-ss20-ub20-launch-catLP-dual-mh-medium-t.mp4"
        ],
        "assigner": {
          "assignerName": "",
          "assignerID": "",
          "assineePhone": "",
          "assignDate": "12/14/2019, 12:37:21 PM"
        },
        "assignee": {
          "assigneeName": "",
          "assigneeID": "",
          "assigneePhone": "",
          "avatar": "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "workDate": "12/14/2019, 12:37:21 PM"
        }
      },
      {
        "chosenServices": [
          "serviceID1, serviceID2"
        ],
        "expectedTime": "12/14/2019, 12:37:21 PM",
        "customerAddress": "42 pham ngoc thach",
        "customerName": "nguyen van a",
        "customerPhone": "0900000001",
        "note": "vui long gọi truoc 30p khi den",
        "payment": {
          "paymentType": "cash",
          "paymentStatusCode": 0,
          "paymentStatusName": "chưa thanh toán",
          "paymentDate": ""
        },
        "order": {
          "orderStatusCode": 3,
          "orderStatusName": "Chờ giao dịch",
          "orderID": "123456",
          "orderValue": {
            "sum": "1,900,000",
            "details": [
              {
                "sericeID": "123456",
                "serviceName": "bảo trì máy lạnh",
                "charge": "1,000,000"
              },
              {
                "sericeID": "123456",
                "serviceName": "Sửa điện thoại",
                "charge": "900,000"
              }
            ]
          }
        },
        "imgList": [
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg"
        ],
        "videoList": [
          "https://brand.assets.adidas.com/video/upload/q_auto,vc_auto/video/upload/global%20brand%20publishing/Running/UB20/running-ss20-ub20-launch-catLP-dual-mh-medium-t.mp4"
        ],
        "assigner": {
          "assignerName": "",
          "assignerID": "",
          "assineePhone": "",
          "assignDate": "12/14/2019, 12:37:21 PM"
        },
        "assignee": {
          "assigneeName": "",
          "assigneeID": "",
          "assigneePhone": "",
          "avatar": "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "workDate": "12/14/2019, 12:37:21 PM"
        }
      },
      {
        "chosenServices": [
          "serviceID1, serviceID2"
        ],
        "expectedTime": "12/14/2019, 12:37:21 PM",
        "customerAddress": "42 pham ngoc thach",
        "customerName": "nguyen van a",
        "customerPhone": "0900000001",
        "note": "vui long gọi truoc 30p khi den",
        "payment": {
          "paymentType": "cash",
          "paymentStatusCode": 0,
          "paymentStatusName": "chưa thanh toán",
          "paymentDate": ""
        },
        "order": {
          "orderStatusCode": 4,
          "orderStatusName": "Hoàn tất",
          "orderID": "123456",
          "orderValue": {
            "sum": "1,900,000",
            "details": [
              {
                "sericeID": "123456",
                "serviceName": "bảo trì máy lạnh",
                "charge": "1,000,000"
              },
              {
                "sericeID": "123456",
                "serviceName": "Sửa điện thoại",
                "charge": "900,000"
              }
            ]
          }
        },
        "imgList": [
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg"
        ],
        "videoList": [
          "https://brand.assets.adidas.com/video/upload/q_auto,vc_auto/video/upload/global%20brand%20publishing/Running/UB20/running-ss20-ub20-launch-catLP-dual-mh-medium-t.mp4"
        ],
        "assigner": {
          "assignerName": "",
          "assignerID": "",
          "assineePhone": "",
          "assignDate": "12/14/2019, 12:37:21 PM"
        },
        "assignee": {
          "assigneeName": "",
          "assigneeID": "",
          "assigneePhone": "",
          "avatar": "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "workDate": "12/14/2019, 12:37:21 PM"
        }
      },
      {
        "chosenServices": [
          "serviceID1, serviceID2"
        ],
        "expectedTime": "12/14/2019, 12:37:21 PM",
        "customerAddress": "42 pham ngoc thach",
        "customerName": "nguyen van a",
        "customerPhone": "0900000001",
        "note": "vui long gọi truoc 30p khi den",
        "payment": {
          "paymentType": "cash",
          "paymentStatusCode": 0,
          "paymentStatusName": "chưa thanh toán",
          "paymentDate": ""
        },
        "order": {
          "orderStatusCode": 5,
          "orderStatusName": "Pending",
          "orderID": "123456",
          "orderValue": {
            "sum": "1,900,000",
            "details": [
              {
                "sericeID": "123456",
                "serviceName": "bảo trì máy lạnh",
                "charge": "1,000,000"
              },
              {
                "sericeID": "123456",
                "serviceName": "Sửa điện thoại",
                "charge": "900,000"
              }
            ]
          }
        },
        "imgList": [
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg"
        ],
        "videoList": [
          "https://brand.assets.adidas.com/video/upload/q_auto,vc_auto/video/upload/global%20brand%20publishing/Running/UB20/running-ss20-ub20-launch-catLP-dual-mh-medium-t.mp4"
        ],
        "assigner": {
          "assignerName": "",
          "assignerID": "",
          "assineePhone": "",
          "assignDate": "12/14/2019, 12:37:21 PM"
        },
        "assignee": {
          "assigneeName": "",
          "assigneeID": "",
          "assigneePhone": "",
          "avatar": "https://www.lyfemarketing.com/blog/wp-content/uploads/2018/08/social-media-video.jpg",
          "workDate": "12/14/2019, 12:37:21 PM"
        }
      }
    ]
  }
