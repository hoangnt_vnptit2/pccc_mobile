import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { BookPage } from '../pages/book/book';
import { TabsPage } from '../pages/tabs/tabs';
import { ServiceProvider } from '../services/GlobalVar';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file/ngx';
import { MediaCapture } from '@ionic-native/media-capture';
import { HistoryBookPage } from '../pages/history-book/history-book';
import { NotificationPage } from '../pages/notification/notification';
import { ServicesPricePage } from '../pages/services-price/services-price';
import { DetailHistoryPage } from '../pages/detail-history/detail-history';
import { ContactPage } from '../pages/contact/contact';
import { Camera, CameraOptions } from '@ionic-native/camera';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    BookPage,
    TabsPage,
    HistoryBookPage,
    NotificationPage,
    ServicesPricePage,
    DetailHistoryPage,
    ContactPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BookPage,
    TabsPage,
    HistoryBookPage,
    NotificationPage,
    ServicesPricePage,
    DetailHistoryPage,
    ContactPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ServiceProvider,
    ImagePicker,
    File,
    Camera,
    MediaCapture,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
